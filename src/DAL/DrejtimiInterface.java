/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Drejtimi;
import java.util.List;

/**
 *
 * @author elina
 */
public interface DrejtimiInterface {
     void create(Drejtimi sh) throws CrudFormException;
    void edit(Drejtimi sh) throws CrudFormException;
    void delete(Drejtimi sh) throws CrudFormException;
    List<Drejtimi> findAll() throws CrudFormException;
    Drejtimi findByID(Integer ID);
}
