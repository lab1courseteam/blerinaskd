/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Statusi;
import java.util.List;
import javax.persistence.Query;


public class StatusiRepository extends EntMngClass implements StatusiInterface {

    public void create(Statusi gr) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(gr);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void edit(Statusi gr) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(gr);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(Statusi gr) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(gr);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public List<Statusi> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Statusi.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    public Statusi findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Statusi e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Statusi)query.getSingleResult();
    }
}