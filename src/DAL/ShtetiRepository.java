/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Shteti;
import java.util.List;
import javax.persistence.Query;

public class ShtetiRepository extends EntMngClass implements ShtetiInterface {    
public void create(Shteti sh) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(sh);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void edit(Shteti sh) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(sh);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(Shteti sh) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(sh);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public List<Shteti> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Shteti.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Shteti findByID(Integer ID) {
       Query query = em.createQuery("SELECT e FROM Shteti e WHERE e.ID = :id");
        query.setParameter("id", ID);
        return (Shteti)query.getSingleResult();
    }
    }
