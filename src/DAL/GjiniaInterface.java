/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Gjinia;
import java.util.List;

/**
 *
 * @author elina
 */
public interface GjiniaInterface {
     void create(Gjinia sh) throws CrudFormException;
    void edit(Gjinia sh) throws CrudFormException;
    void delete(Gjinia sh) throws CrudFormException;
    List<Gjinia> findAll() throws CrudFormException;
    Gjinia findByID(Integer ID);
}
