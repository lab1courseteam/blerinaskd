/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Drejtimi;
import java.util.List;
import javax.persistence.Query;

public class DrejtimiRepository extends EntMngClass implements DrejtimiInterface {    
public void create(Drejtimi sh) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(sh);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void edit(Drejtimi sh) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(sh);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(Drejtimi sh) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(sh);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public List<Drejtimi> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Drejtimi.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Drejtimi findByID(Integer ID) {
       Query query = em.createQuery("SELECT e FROM Drejtimi e WHERE e.ID = :id");
        query.setParameter("id", ID);
        return (Drejtimi)query.getSingleResult();
    }
    }
