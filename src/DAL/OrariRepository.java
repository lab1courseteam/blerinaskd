/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Orari;
import java.util.List;
import javax.persistence.Query;

public class OrariRepository extends EntMngClass implements OrariInterface {

    public void create(Orari gr) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(gr);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void edit(Orari gr) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(gr);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(Orari gr) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(gr);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public List<Orari> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Orari.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    public Orari findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Orari e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Orari)query.getSingleResult();
    }


}