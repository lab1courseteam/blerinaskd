/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;
import BLL.Statusi;
import java.util.List;



public interface StatusiInterface {
     void create(Statusi gr) throws CrudFormException;
    void edit(Statusi gr) throws CrudFormException;
    void delete(Statusi gr) throws CrudFormException;
    List<Statusi> findAll() throws CrudFormException;
    Statusi findByID(Integer ID);
}
