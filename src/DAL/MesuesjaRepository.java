/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Mesuesja;
import java.util.List;
import javax.persistence.Query;

public class MesuesjaRepository extends EntMngClass implements MesuesjaInterface {    
@Override
public void create(Mesuesja sh) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(sh);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void edit(Mesuesja sh) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(sh);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(Mesuesja sh) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(sh);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public List<Mesuesja> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Mesuesja.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Mesuesja findByID(Integer ID) {
       Query query = em.createQuery("SELECT e FROM Mesuesja e WHERE e.ID = :id");
        query.setParameter("id", ID);
        return (Mesuesja)query.getSingleResult();
    }
}