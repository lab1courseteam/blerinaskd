/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Qyteti;
import java.util.List;
import javax.persistence.Query;

public class QytetiRepository extends EntMngClass implements QytetiInterface {    
public void create(Qyteti sh) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(sh);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void edit(Qyteti sh) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(sh);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(Qyteti sh) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(sh);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public List<Qyteti> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Qyteti.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Qyteti findByID(Integer ID) {
       Query query = em.createQuery("SELECT e FROM Qyteti e WHERE e.ID = :id");
        query.setParameter("id", ID);
        return (Qyteti)query.getSingleResult();
    }
    }