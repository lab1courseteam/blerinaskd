/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Gjinia;
import java.util.List;
import javax.persistence.Query;

public class GjiniaRepository extends EntMngClass implements GjiniaInterface {    
public void create(Gjinia sh) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(sh);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void edit(Gjinia sh) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(sh);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(Gjinia sh) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(sh);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public List<Gjinia> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Gjinia.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Gjinia findByID(Integer ID) {
       Query query = em.createQuery("SELECT e FROM Gjinia e WHERE e.ID = :id");
        query.setParameter("id", ID);
        return (Gjinia)query.getSingleResult();
    }
    }
