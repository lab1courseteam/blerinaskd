/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.GrupMosha;
import java.util.List;

/**
 *
 * @author elina
 */
public interface GrupMoshaInterface {
     void create(GrupMosha gm) throws CrudFormException;
    void edit(GrupMosha gm) throws CrudFormException;
    void delete(GrupMosha gm) throws CrudFormException;
    List<GrupMosha> findAll() throws CrudFormException;
    GrupMosha findByID(Integer ID);
}
