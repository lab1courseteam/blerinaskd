/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Grupi;
import java.util.List;

/**
 *
 * @author elina
 */
public interface GrupiInterface {
       void create(Grupi gr) throws CrudFormException;
    void edit(Grupi gr) throws CrudFormException;
    void delete(Grupi gr) throws CrudFormException;
    List<Grupi> findAll() throws CrudFormException;
    Grupi findByID(Integer ID);
}
