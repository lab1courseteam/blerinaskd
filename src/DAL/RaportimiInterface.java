/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Raportimi;
import java.util.List;

/**
 *
 * @author elina
 */
public interface RaportimiInterface {
      void create(Raportimi gr) throws CrudFormException;
    void edit(Raportimi gr) throws CrudFormException;
    void delete(Raportimi gr) throws CrudFormException;
    List<Raportimi> findAll() throws CrudFormException;
    Raportimi findByID(Integer ID);
}
