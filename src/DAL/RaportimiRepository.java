/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Raportimi;
import java.util.List;
import javax.persistence.Query;

public class RaportimiRepository extends EntMngClass implements RaportimiInterface {

    public void create(Raportimi gr) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(gr);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void edit(Raportimi gr) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(gr);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(Raportimi gr) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(gr);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public List<Raportimi> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Raportimi.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    public Raportimi findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Raportimi e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Raportimi)query.getSingleResult();
    }
}
