/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Mesuesja;
import java.util.List;

/**
 *
 * @author elina
 */
public interface MesuesjaInterface {
       void create(Mesuesja sh) throws CrudFormException;
    void edit(Mesuesja sh) throws CrudFormException;
    void delete(Mesuesja sh) throws CrudFormException;
    List<Mesuesja> findAll() throws CrudFormException;
    Mesuesja findByID(Integer ID);
}
