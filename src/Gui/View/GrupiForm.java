/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.View;

import BLL.Grupi;
import BLL.GrupMosha;
import DAL.CrudFormException;
import DAL.GrupMoshaRepository;
import DAL.GrupiInterface;
import DAL.GrupiRepository;
import Gui.Model.GrupiTableModel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import Gui.Model.GrupMoshaCombobox;

public class GrupiForm extends javax.swing.JInternalFrame {
GrupiInterface gi = new GrupiRepository();
GrupiTableModel gtm = new GrupiTableModel();
 GrupMoshaRepository grp = new GrupMoshaRepository();
    GrupMoshaCombobox  bcb = new GrupMoshaCombobox();


    public GrupiForm()throws CrudFormException {
        initComponents();
        loadTable();
        tabelaSelectedIndexChange();
        loadComboBox1();
    }
     
 public void loadTable() throws CrudFormException {
     
            List<Grupi> lista = gi.findAll();
            gtm.add(lista);
            tabela.setModel(gtm);
            gtm.fireTableDataChanged();
        
 }
   public void loadComboBox1() {
        try {
            List<GrupMosha> lista = grp.findAll();
            bcb.add(lista);
            ComboBox1.setModel(bcb);
            ComboBox1.repaint();
        } catch (CrudFormException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        }

    }
     private void tabelaSelectedIndexChange() {
		final ListSelectionModel rowSM = tabela.getSelectionModel();
		rowSM.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent Ise) {
                if (Ise.getValueIsAdjusting()) {
                    return;
                }
                ListSelectionModel rowSM = (ListSelectionModel) Ise.getSource();
                int selectedIndex = rowSM.getAnchorSelectionIndex();
                if (selectedIndex > -1) {
                    Grupi gr = gtm.getGrupi(selectedIndex);

                    txtid.setText(gr.getGrupiID() + "");
                    txtgrupi.setText(gr.getGrupi());
                     bcb.setSelectedItem(gr.getGrupMoshaID());
                    ComboBox1.repaint();

                }
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtgrupi = new javax.swing.JTextField();
        save = new javax.swing.JButton();
        edit = new javax.swing.JButton();
        delete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabela = new javax.swing.JTable();
        txtid = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        ComboBox1 = new javax.swing.JComboBox();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jLabel1.setText("Grupi ID");

        jLabel2.setText("Grupi");

        txtgrupi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtgrupiActionPerformed(evt);
            }
        });

        save.setText("Save");
        save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveActionPerformed(evt);
            }
        });

        edit.setText("Edit");
        edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editActionPerformed(evt);
            }
        });

        delete.setText("Delete");
        delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteActionPerformed(evt);
            }
        });

        tabela.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Grupi ID", "Grupi", "GrupMosha "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabela);

        txtid.setEditable(false);
        txtid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtidActionPerformed(evt);
            }
        });

        jButton1.setText("Clear");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel3.setText("GrupMosha ID ");

        ComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtgrupi, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtid, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(save, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(edit, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(delete, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtgrupi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(save)
                    .addComponent(edit)
                    .addComponent(jButton1)
                    .addComponent(delete))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteActionPerformed
        int row = tabela.getSelectedRow();
        if (row != -1) {
            Object[] ob = {"Po", "Jo"};
            int i = 
			JOptionPane.showOptionDialog(this, "A dëshironi ta fshini ?", "Fshirja", 
			JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, ob, ob[1]);
            if (i == 0) {
                Grupi gr = gtm.getGrupi (row);
                try {
                    gi.delete(gr);
                } catch (CrudFormException ex) {
                    Logger.getLogger(GrupiForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                clearField();
                try {
                    loadTable();
                } catch (CrudFormException ex) {
                    Logger.getLogger(GrupiForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
               clearField();
            }
        }else{
            JOptionPane.showMessageDialog(this, "Nuk keni selektuar asgje per te fshire!");
        }
    }//GEN-LAST:event_deleteActionPerformed

    private void editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editActionPerformed
                 int row = tabela.getSelectedRow();
        if (!txtgrupi.getText().equals("")) {
            if (row == -1) {
                Grupi gr = new Grupi();
                gr.setGrupiID(Integer.parseInt(txtid.getText()));
                gr.setGrupi(txtgrupi.getText());
                gr.setGrupMoshaID((GrupMosha) bcb.getSelectedItem());
           
             

                try {
                    gi.create(gr);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }

            } else {
                Grupi gr = gtm.getGrupi(row);
                gr.setGrupi(txtgrupi.getText());
                gr.setGrupMoshaID((GrupMosha) bcb.getSelectedItem());
               

                try {
                    gi.edit( gr );
                } catch (CrudFormException ex) {
                    Logger.getLogger(GrupiForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                 clearField();
                loadTable();
            } catch (CrudFormException ex) {
                Logger.getLogger(GrupiForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_editActionPerformed

    private void txtgrupiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtgrupiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtgrupiActionPerformed

    private void saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveActionPerformed
        int row = tabela.getSelectedRow();
        if (!txtgrupi.getText().equals("")) {
            if (row == -1) {
                 Grupi gr = new Grupi();
                gr.setGrupiID(Integer.parseInt(txtid.getText()));
                gr.setGrupi(txtgrupi.getText());
                gr.setGrupMoshaID((GrupMosha) bcb.getSelectedItem());
                 

                try {
                    gi.create(gr);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }

            } else {
               Grupi gr = gtm.getGrupi(row);
                gr.setGrupi(txtgrupi.getText());
                gr.setGrupMoshaID((GrupMosha) bcb.getSelectedItem());

                try{
                gi.edit(gr);
            }catch (CrudFormException  ex){
                Logger.getLogger(GrupiForm.class.getName()).log(Level.SEVERE,null,ex);
            }
            }
            clearField();
                   try {
                       loadTable();
                   } catch (CrudFormException ex) {
                       Logger.getLogger(GrupiForm.class.getName()).log(Level.SEVERE, null, ex);
                   }
        } else {
            JOptionPane.showMessageDialog(this, "Ju lutem plotesoni fushat obligative (me shenjen *)!");

        }
    }//GEN-LAST:event_saveActionPerformed

    private void txtidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtidActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        clearField();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void ComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboBox1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox ComboBox1;
    private javax.swing.JButton delete;
    private javax.swing.JButton edit;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton save;
    private javax.swing.JTable tabela;
    private javax.swing.JTextField txtgrupi;
    private javax.swing.JTextField txtid;
    // End of variables declaration//GEN-END:variables

    private void clearField() {
          txtid.setText("");
        txtgrupi.setText("");
        ComboBox1.setSelectedIndex(-1);
        ComboBox1.repaint();
    }
    
}
