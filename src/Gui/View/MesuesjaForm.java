/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.View;

import BLL.Drejtimi;
import BLL.Gjinia;
import BLL.Mesuesja;
import BLL.Qyteti;
import BLL.Shteti;
import DAL.CrudFormException;
import DAL.DrejtimiRepository;
import DAL.GjiniaRepository;
import DAL.MesuesjaInterface;
import DAL.MesuesjaRepository;
import DAL.QytetiRepository;
import DAL.ShtetiRepository;
import Gui.Model.DrejtimiCombobox;
import Gui.Model.GjiniaCombobox;
import Gui.Model.MesuesjaTableModel;
import Gui.Model.QytetiCombobox;
import Gui.Model.ShtetiCombobox;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MesuesjaForm extends javax.swing.JInternalFrame {
 MesuesjaInterface shi = new MesuesjaRepository();
    MesuesjaTableModel shtm = new MesuesjaTableModel();
  QytetiRepository grp = new QytetiRepository();
  ShtetiRepository srp = new ShtetiRepository();
  GjiniaRepository jrp = new GjiniaRepository();
  DrejtimiRepository drp = new DrejtimiRepository();
  
    QytetiCombobox  bcb = new QytetiCombobox() ;
    ShtetiCombobox scs = new ShtetiCombobox() ;
    GjiniaCombobox gcg = new GjiniaCombobox();
    DrejtimiCombobox dcd = new DrejtimiCombobox();
    
    
    

    public MesuesjaForm()throws CrudFormException {
        initComponents();
        loadTable();
         tabelaSelectedIndexChange();
        loadComboBox1();
    }

       public void loadTable() throws CrudFormException{
        try {
            List<Mesuesja> lista = shi.findAll();
            shtm.add(lista);
            tabela.setModel(shtm);
            shtm.fireTableDataChanged();
        } catch (CrudFormException ene) {
            JOptionPane.showMessageDialog(this, ene.getMessage());
        }

    }
         public void loadComboBox1() {
        try {
            List<Qyteti> lista = grp.findAll();
            bcb.add(lista);
            combobox1.setModel(bcb);
            combobox1.repaint(); 
            List<Shteti> lista1 = srp.findAll();
            scs.add(lista1);
            combobox2.setModel(scs);
            combobox2.repaint();
             List<Gjinia> lista2 = jrp.findAll();
            gcg.add(lista2);
            combobox3.setModel(gcg);
            combobox3.repaint();
             List<Drejtimi> lista3 = drp.findAll();
           dcd.add(lista3);
            combobox4.setModel(dcd);
            combobox4.repaint();
        } catch (CrudFormException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        }

    }
        private void tabelaSelectedIndexChange() {
        final ListSelectionModel rowSM = tabela.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent Ise) {
                if (Ise.getValueIsAdjusting()) {
                    return;
                }
                ListSelectionModel rowSM = (ListSelectionModel) Ise.getSource();
                int selectedIndex = rowSM.getAnchorSelectionIndex();
                if (selectedIndex > -1) {
                    Mesuesja en = shtm.getMesuesja(selectedIndex);
                    txt1.setText(en.getMid() + "");
                    txt2.setText(en.getEmri());
                    txt3.setText(en.getMbiemri());
                    txt4.setText(en.getUsername());
                    txt5.setText(en.getPassword());
                    bcb.setSelectedItem(en.getQyteti());
                    combobox1.repaint();
                    scs.setSelectedItem(en.getShteti());
                    combobox2.repaint();
                    gcg.setSelectedItem(en.getGjinia());
                    combobox3.repaint();
                    dcd.setSelectedItem(en.getGjinia());
                    combobox4.repaint();
                }
            }

        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txt1 = new javax.swing.JTextField();
        txt2 = new javax.swing.JTextField();
        txt3 = new javax.swing.JTextField();
        combobox1 = new javax.swing.JComboBox();
        combobox2 = new javax.swing.JComboBox();
        combobox3 = new javax.swing.JComboBox();
        combobox4 = new javax.swing.JComboBox();
        save = new javax.swing.JButton();
        edit = new javax.swing.JButton();
        clear = new javax.swing.JButton();
        delete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabela = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txt4 = new javax.swing.JTextField();
        txt5 = new javax.swing.JPasswordField();

        jLabel2.setText("jLabel2");

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jLabel1.setText("Mesuesja ID");

        jLabel3.setText("Emri");

        jLabel4.setText("Mbiemri");

        jLabel5.setText("Qyteti");

        jLabel6.setText("Shteti");

        jLabel7.setText("Gjinia");

        jLabel8.setText("Drejtimi ID");

        txt1.setEditable(false);

        combobox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        combobox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        combobox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        combobox4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        save.setText("Save");
        save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveActionPerformed(evt);
            }
        });

        edit.setText("Edit");
        edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editActionPerformed(evt);
            }
        });

        clear.setText("Clear");
        clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearActionPerformed(evt);
            }
        });

        delete.setText("Delete");
        delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteActionPerformed(evt);
            }
        });

        tabela.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Mesuesja ID", "Emri", "Mbiemri", "Gjinia", "Qyteti", "Shteti", "Drejtimi ID"
            }
        ));
        jScrollPane1.setViewportView(tabela);

        jLabel9.setText("Username");

        jLabel10.setText("Password");

        txt4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE))
                        .addGap(70, 70, 70)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txt1)
                            .addComponent(txt2)
                            .addComponent(txt3)
                            .addComponent(combobox1, 0, 137, Short.MAX_VALUE)
                            .addComponent(combobox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(combobox3, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(combobox4, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt4)
                            .addComponent(txt5))
                        .addGap(18, 18, 18)
                        .addComponent(save, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(edit, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(clear, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(delete, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 17, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(combobox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(combobox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(combobox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(combobox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txt4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(save)
                    .addComponent(edit)
                    .addComponent(clear)
                    .addComponent(delete)
                    .addComponent(txt5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveActionPerformed
                   int row = tabela.getSelectedRow();
        if (!txt2.getText().equals("")) {
            if (row == -1) {
                 Mesuesja sh = new Mesuesja();
                //sh.setShID(Integer.parseInt(txt1.getText()));
                sh.setEmri(txt2.getText());
                sh.setMbiemri(txt3.getText());
                sh.setUsername(txt4.getText());
                sh.setPassword(txt5.getText());
                sh.setQyteti((Qyteti) bcb.getSelectedItem());
                sh.setShteti((Shteti) scs.getSelectedItem());
                sh.setGjinia((Gjinia) gcg.getSelectedItem());
                sh.setDrejtimiID((Drejtimi) dcd.getSelectedItem());
                try {
                    shi.create(sh);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }

            } else {
               Mesuesja sh = shtm.getMesuesja(row);
                sh.setEmri(txt2.getText());
                sh.setMbiemri(txt3.getText());
                sh.setUsername(txt4.getText());
                sh.setPassword(txt5.getText());
                sh.setQyteti((Qyteti) bcb.getSelectedItem());
                sh.setShteti((Shteti) scs.getSelectedItem());
                sh.setGjinia((Gjinia) gcg.getSelectedItem());
                sh.setDrejtimiID((Drejtimi) dcd.getSelectedItem());
           try{
                shi.edit(sh);
            }catch (CrudFormException  ex){
                Logger.getLogger(MesuesjaForm.class.getName()).log(Level.SEVERE,null,ex);
            }
            }
            clearField();
                       try {
                           loadTable();
                       } catch (CrudFormException ex) {
                           Logger.getLogger(MesuesjaForm.class.getName()).log(Level.SEVERE, null, ex);
                       }
        } else {
            JOptionPane.showMessageDialog(this, "Ju lutem plotesoni fushat obligative (me shenjen *)!");

        }
    }//GEN-LAST:event_saveActionPerformed

    private void clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearActionPerformed
      clearField();
    }//GEN-LAST:event_clearActionPerformed

    private void editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editActionPerformed
                  int row = tabela.getSelectedRow();
        if (!txt2.getText().equals("")) {
            if (row == -1) {
                 Mesuesja sh = new Mesuesja();
                //sh.setShID(Integer.parseInt(txt1.getText()));
                sh.setEmri(txt2.getText());
                sh.setMbiemri(txt3.getText());
                sh.setUsername(txt4.getText());
                sh.setPassword(txt5.getText());
                sh.setQyteti((Qyteti) bcb.getSelectedItem());
                sh.setShteti((Shteti) scs.getSelectedItem());
                sh.setGjinia((Gjinia) gcg.getSelectedItem());
                sh.setDrejtimiID((Drejtimi) dcd.getSelectedItem());

                try {
                    shi.create(sh);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }

            } else {
               Mesuesja sh = shtm.getMesuesja(row);
                sh.setEmri(txt2.getText());
                sh.setMbiemri(txt3.getText());
                sh.setUsername(txt4.getText());
                sh.setPassword(txt5.getText());
                sh.setQyteti((Qyteti) bcb.getSelectedItem());
                sh.setShteti((Shteti) scs.getSelectedItem());
                sh.setGjinia((Gjinia) gcg.getSelectedItem());
                 sh.setDrejtimiID((Drejtimi) dcd.getSelectedItem());
           try{
                shi.edit(sh);
            }catch (CrudFormException  ex){
                Logger.getLogger(MesuesjaForm.class.getName()).log(Level.SEVERE,null,ex);
            }
            }
            clearField();
                      try {
                          loadTable();
                      } catch (CrudFormException ex) {
                          Logger.getLogger(MesuesjaForm.class.getName()).log(Level.SEVERE, null, ex);
                      }
        } else {
            JOptionPane.showMessageDialog(this, "Ju lutem plotesoni fushat obligative (me shenjen *)!");

        }
    }//GEN-LAST:event_editActionPerformed

    private void deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteActionPerformed
         int row = tabela.getSelectedRow();
        if (row != -1) {
            Object[] ob = {"Po", "Jo"};
            int i = 
			JOptionPane.showOptionDialog(this, "A dëshironi ta fshini ?", "Fshirja", 
			JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, ob, ob[1]);
            if (i == 0) {
                Mesuesja sh = shtm.getMesuesja (row);
                try {
                    shi.delete(sh);
                } catch (CrudFormException ex) {
                    Logger.getLogger(MesuesjaForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                clearField();
                try {
                    loadTable();
                } catch (CrudFormException ex) {
                    Logger.getLogger(MesuesjaForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
               clearField();
            }
        }else{
            JOptionPane.showMessageDialog(this, "Nuk keni selektuar asgje per te fshire!");
        }
    }//GEN-LAST:event_deleteActionPerformed

    private void txt4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt4ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clear;
    private javax.swing.JComboBox combobox1;
    private javax.swing.JComboBox combobox2;
    private javax.swing.JComboBox combobox3;
    private javax.swing.JComboBox combobox4;
    private javax.swing.JButton delete;
    private javax.swing.JButton edit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton save;
    private javax.swing.JTable tabela;
    private javax.swing.JTextField txt1;
    private javax.swing.JTextField txt2;
    private javax.swing.JTextField txt3;
    private javax.swing.JTextField txt4;
    private javax.swing.JPasswordField txt5;
    // End of variables declaration//GEN-END:variables

    private void clearField() {
         txt1.setText("");
         txt2.setText("");
         txt3.setText("");
         txt4.setText("");
         combobox1.setSelectedIndex(-1);
        combobox1.repaint();
        combobox2.setSelectedIndex(-1);
        combobox2.repaint();
        combobox3.setSelectedIndex(-1);
        combobox3.repaint();
        combobox4.setSelectedIndex(-1);
        combobox4.repaint();
    }

  
}
