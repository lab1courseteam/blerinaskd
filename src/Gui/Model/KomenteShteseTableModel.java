/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;

import BLL.KomenteShtese;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author elina
 */
public class KomenteShteseTableModel extends AbstractTableModel{
      private final String [] columnNames = {"ShID","Shendeti","Terapia",};
 
    private List <KomenteShtese> data;
    public KomenteShteseTableModel(List<KomenteShtese>data){
        this.data = data;
    }
    public KomenteShteseTableModel() {
    }
    public void add(List<KomenteShtese>data){
        this.data = data;
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public KomenteShtese getKomenteShtese(int index){
        return data.get(index);
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        KomenteShtese sh = (KomenteShtese)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return sh.getShID();
            case 1:
                return sh.getShendeti();
            case 2:
                return sh.getTerapia();
            default:
                return null;
        }
    }
}
