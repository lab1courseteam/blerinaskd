/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;

import BLL.Orari;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author elina
 */
public class OrariTableModel extends AbstractTableModel{
      private final String [] columnNames = {"OrariID","EID","SallaID","OraFillimit","OraMbarimit","Pauza","DataFillimit","DataMbarimit"};
 
    private List <Orari> data;
    public OrariTableModel(List<Orari>data){
        this.data = data;
    }
    public OrariTableModel() {
    }
    public void add(List<Orari>data){
        this.data = data;
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Orari getOrari(int index){
        return data.get(index);
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Orari sh = (Orari)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return sh.getOrariID();
            case 1:
                return sh.getEid();
            case 2:
                return sh.getSallaID();
                 case 3:
                return sh.getOraFillimit();
            case 4:
                return sh.getOraMbarimit();
            case 5:
                return sh.getPauza();
                 case 6:
                return sh.getDataFillimit();
            case 7:
                return sh.getDataMbarimit();
            default:
                return null;
        }
    }
}