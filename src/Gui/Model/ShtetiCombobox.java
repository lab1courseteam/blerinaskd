/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;

import BLL.Shteti;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author elina
 */
public class ShtetiCombobox extends AbstractListModel<Shteti> implements ComboBoxModel<Shteti> {
        private List<Shteti> data;
    private Shteti selectedItem;

    public ShtetiCombobox(List<Shteti> data) {
        this.data = data;
    }

    public ShtetiCombobox() {
    }

    public void add(List<Shteti> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Shteti getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(Shteti)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}
