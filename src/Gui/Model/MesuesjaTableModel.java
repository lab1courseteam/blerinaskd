/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;

import BLL.Mesuesja;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class MesuesjaTableModel extends AbstractTableModel {
      private final String [] columnNames = {"MID","Emri","Mbiemri","Qyteti","Shteti","Gjinia","DrejtimiID","Username","Password"};
 
    private List <Mesuesja> data;
    public MesuesjaTableModel(List<Mesuesja>data){
        this.data = data;
    }
    public MesuesjaTableModel() {
    }
    public void add(List<Mesuesja>data){
        this.data = data;
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Mesuesja getMesuesja(int index){
        return data.get(index);
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Mesuesja gm = (Mesuesja)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return gm.getMid();
            case 1:
                return gm.getEmri();
            case 2:
                return gm.getMbiemri();
            case 3:
                return gm.getQyteti();
            case 4:
                return gm.getShteti();
                case 5:
                return gm.getGjinia();
            case 6:
                return gm.getDrejtimiID();
            case 7:
                return gm.getUsername();
            case 8:
                return gm.getPassword();
            default:
                return null;
        }
    }
}
