/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;

import BLL.Qyteti;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author elina
 */
public class QytetiTableModel extends AbstractTableModel{
      private final String [] columnNames = {"QID","Emri","KodiPostar"};
 
    private List <Qyteti> data;
    public QytetiTableModel(List<Qyteti>data){
        this.data = data;
    }
    public QytetiTableModel() {
    }
    public void add(List<Qyteti>data){
        this.data = data;
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Qyteti getQyteti(int index){
        return data.get(index);
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Qyteti sh = (Qyteti)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return sh.getQid();
            case 1:
                return sh.getEmri();
            case 2:
                return sh.getKodiPostar();
            default:
                return null;
        }
    }
    
}
