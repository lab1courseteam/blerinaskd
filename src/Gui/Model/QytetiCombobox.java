/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;

import BLL.Qyteti;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author elina
 */
public class QytetiCombobox extends AbstractListModel<Qyteti> implements ComboBoxModel<Qyteti> {
        private List<Qyteti> data;
    private Qyteti selectedItem;

    public QytetiCombobox(List<Qyteti> data) {
        this.data = data;
    }

    public QytetiCombobox() {
    }

    public void add(List<Qyteti> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Qyteti getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(Qyteti)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}
