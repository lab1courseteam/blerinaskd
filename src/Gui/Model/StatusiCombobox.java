/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;

import BLL.Statusi;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class StatusiCombobox extends AbstractListModel<Statusi> implements ComboBoxModel<Statusi> {
        private List<Statusi> data;
    private Statusi selectedItem;

    public StatusiCombobox(List<Statusi> data) {
        this.data = data;
    }

    public StatusiCombobox() {
    }

    public void add(List<Statusi> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Statusi getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(Statusi)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }

    
}