/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;

import BLL.Statusi;
import java.util.List;
import javax.swing.table.AbstractTableModel;


    public class StatusiTableModel extends AbstractTableModel {
    
    private final String [] columnNames = {"StatusiID","Emertimi"};
 
    private List <Statusi> data;
    public StatusiTableModel(List<Statusi>data){
        this.data = data;
    }
    public StatusiTableModel() {
    }
    public void add(List<Statusi>data){
        this.data = data;
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Statusi getStatusi(int index){
        return data.get(index);
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Statusi gr = (Statusi)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return gr.getStatusiID();
            case 1:
                return gr.getEmertimi();
            default:
                return null;
        }
    }
}

