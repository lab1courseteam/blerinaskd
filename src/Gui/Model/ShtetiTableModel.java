/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;

import BLL.Shteti;
import java.util.List;
import javax.swing.table.AbstractTableModel;


    public class ShtetiTableModel extends AbstractTableModel{
      private final String [] columnNames = {"ShID","Emri","KodiPostar",};
 
    private List <Shteti> data;
    public ShtetiTableModel(List<Shteti>data){
        this.data = data;
    }
    public ShtetiTableModel() {
    }
    public void add(List<Shteti>data){
        this.data = data;
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Shteti getShteti(int index){
        return data.get(index);
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Shteti sh = (Shteti)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return sh.getShID();
            case 1:
                return sh.getEmri();
            case 2:
                return sh.getKodiPostar();
            default:
                return null;
        }
    }
}
