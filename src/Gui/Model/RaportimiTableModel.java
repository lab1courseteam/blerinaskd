/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;

import BLL.Raportimi;
import BLL.Statusi;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class RaportimiTableModel extends AbstractTableModel {
    
    private final String [] columnNames = {"RaportimiID","OrariID","Komenti","StatusiID","Data"};
 
    private List <Raportimi> data;
    public RaportimiTableModel(List<Raportimi>data){
        this.data = data;
    }
    public RaportimiTableModel() {
    }
    public void add(List<Raportimi>data){
        this.data = data;
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Raportimi getRaportimi(int index){
        return data.get(index);
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Raportimi gr = (Raportimi)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return gr.getRaportimiID();
            case 1:
                return gr.getOrariID();
            case 2:
                return gr.getKomenti();
            case 3:
                return gr.getStatusiID();
            case 4:
                return gr.getData();
            default:
                return null;
        }
    }

  
}