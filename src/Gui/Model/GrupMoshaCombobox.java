/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;


import BLL.GrupMosha;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author elina
 */
public class GrupMoshaCombobox extends AbstractListModel<GrupMosha> implements ComboBoxModel<GrupMosha> {
        private List<GrupMosha> data;
    private GrupMosha selectedItem;

    public GrupMoshaCombobox(List<GrupMosha> data) {
        this.data = data;
    }

    public GrupMoshaCombobox() {
    }

    public void add(List<GrupMosha> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public GrupMosha getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(GrupMosha)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}