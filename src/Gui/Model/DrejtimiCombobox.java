/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui.Model;

import BLL.Drejtimi;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author elina
 */
public class DrejtimiCombobox extends AbstractListModel<Drejtimi> implements ComboBoxModel<Drejtimi> {
        private List<Drejtimi> data;
    private Drejtimi selectedItem;

    public DrejtimiCombobox(List<Drejtimi> data) {
        this.data = data;
    }

    public DrejtimiCombobox() {
    }

    public void add(List<Drejtimi> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Drejtimi getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(Drejtimi)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}
